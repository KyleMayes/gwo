from django.db import models

# A member
class Member(models.Model):
    YES = 'Y'
    NO = 'N'
    MAYBE = '?'
    EXCUSED = 'E'
    ATTENDING_CHOICES = (
        (YES, 'Yes'),
        (NO, 'No'),
        (MAYBE, 'Maybe'),
        (EXCUSED, 'Excused'),
    )

    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    nick_name = models.CharField(max_length=32)
    email = models.CharField(max_length=64)
    avatar = models.CharField(max_length=32)
    attending = models.CharField(max_length=1, choices=ATTENDING_CHOICES, default=NO)
    status = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
