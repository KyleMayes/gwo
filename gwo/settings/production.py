# Import common settings
from .common import *

DEBUG = False
SECRET_KEY = 'o90y4jklx7dp24_p^)^mwi=o8jl*_#c(lhx)$d(-n0@gg6-g9m'

# PostgreSQL settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gwo',
        'USER': 'gwo',
        'PASSWORD': 'gigo35',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}
