from django.conf.urls import url
from django.contrib import admin

from gwo import views

urlpatterns = [
    # Administration
    url(r'^admin/', admin.site.urls),
    # Pages
    url(r'^$', views.index, name='index'),
    url(r'^members$', views.members, name='members'),
]
