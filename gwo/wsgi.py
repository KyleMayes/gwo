import os

from django.core.wsgi import get_wsgi_application

# Load settings
settings = 'production' if os.environ.get('DJANGO_DEVELOPMENT') is None else 'development'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gwo.settings.{}'.format(settings))

# Run the application
application = get_wsgi_application()
