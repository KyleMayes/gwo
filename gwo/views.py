import itertools

from django.shortcuts import render

from . import config
from .models import Member

# Returns the default context as defined by the current configuration
def get_context():
    return { 'previous': config.previous, 'upcoming': config.upcoming }

# '/'
def index(request):
    # Load all the current members
    members = Member.objects.all()

    # Count the members that are expected to attend
    attending = len(list(filter(lambda m: m.attending == Member.YES, members)))

    context = get_context()
    context['attending'] = attending

    return render(request, 'index.html', context)

# '/members'
def members(request):
    # Load all the current members
    members = Member.objects.all()

    # Group the members based on their expected attendance
    members = sorted(members, key=lambda m: m.attending)
    groups = itertools.groupby(members, lambda m: m.attending)
    groups = dict((g[0], list(g[1])) for g in groups)

    context = get_context()
    context['members'] = len(members)
    context['yes'] = groups.get(Member.YES, [])
    context['no'] = groups.get(Member.NO, [])
    context['maybe'] = groups.get(Member.MAYBE, [])
    context['excused'] = groups.get(Member.EXCUSED, [])

    return render(request, 'members.html', context)
