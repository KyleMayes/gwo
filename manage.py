#!/usr/bin/env python

import os
import sys

from django.core.management import execute_from_command_line

if __name__ == '__main__':
    # Load settings
    settings = 'production' if os.environ.get('DJANGO_DEVELOPMENT') is None else 'development'
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gwo.settings.{}'.format(settings))

    # Run the application
    execute_from_command_line(sys.argv)
